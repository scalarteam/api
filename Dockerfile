FROM rustlang/rust:nightly

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

EXPOSE 6767

RUN cargo build

CMD ["cargo", "run"]